const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cors = require("cors")({origin: true});

// // Create and deploy your first functions
// // https://firebase.google.com/docs/functions/get-started
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
// The Firebase Admin SDK to access Firestore.

admin.initializeApp();
const db = admin.firestore();

exports.helloWorld = functions.https.onRequest((request, response) =>{
  functions.logger.info("Hello logs!", {structuredData: true});
  cors(request, response, () => {
    functions.logger.info("Hello logs!", {structuredData: true});
    const docRef = db.collection("naruto").doc("QpgKRXQzehcPJ0mxzekd");
    docRef.get().then((doc) => {
      if (!doc.exists) {
        console.log("No such document!");
        return response.send("Not Found");
      }
      console.log(doc.data());
      return response.send(doc.data());
    }). catch((err) => {
      console.log( "Error getting document ", err);
    });
  });
});
